package com.brmlab.android3;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {

	private TextView status_label;
	private LinearLayout buttonLayout;
	AsyncTask<String, Void, Boolean> hovno;
	static final Map<String, String> SPACES = new HashMap<String, String>() {
		private static final long serialVersionUID = 1L;

		{
			put("Brmlab", "http://nat.brmlab.cz/brmd/brmstatus.json");
			put("ACKspace", "https://ackspace.nl/status.php");
			put("Bitlair", "https://bitlair.nl/statejson.php");
			put("NURDSpace", "http://nurdspace.tk/spaceapi/status.json");
			put("Tetalab", "http://status.tetalab.org/status.json");
			put("FamiLAB", "http://familab.org/status/status.php");
			put("UrLab", "http://api.urlab.be/spaceapi/");
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		status_label = (TextView) findViewById(R.id.status_label);
		buttonLayout = (LinearLayout) findViewById(R.id.button_layout);

		for (String key : SPACES.keySet()) {
			Button newButton = new Button(this);
			newButton.setText(key);
			newButton.setTag(SPACES.get(key));
			newButton.setOnClickListener(this);
			buttonLayout.addView(newButton);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public void onClick(View v) {

		if (v instanceof Button
				&& (hovno == null || (hovno.getStatus() != AsyncTask.Status.RUNNING && hovno.getStatus() != AsyncTask.Status.PENDING))) {
			Button b = (Button) v;
			String url = (String) v.getTag();
			if (url != null) {
				hovno = new FetchStatusTask().execute(url, b.getText().toString());
			}
		} else if (v instanceof Button) {
			Toast.makeText(getApplicationContext(), R.string.processing, Toast.LENGTH_SHORT).show();
		}
	}

	private class FetchStatusTask extends AsyncTask<String, Void, Boolean> {

		private String name;

		@Override
		protected void onPreExecute() {
		}

		@Override
		protected Boolean doInBackground(String... params) {

			if (params.length > 0 && params[0] != null) {
				if (params.length > 1) {
					name = params[1];
				}
				try {
					Log.d("Task started for ", params[1] + ", " + params[0]);
					// create HTTP client object
					HttpClient client = new DefaultHttpClient();
					// most simple way to init GET request
					HttpGet request = new HttpGet(params[0]);
					// response will cover whatever response we get
					HttpResponse rp = client.execute(request);
					// we're checking response code to be "200 OK"
					if (rp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
						// EntityUtils is helper from HttpClient implementation,
						// will convert request to String
						String results = EntityUtils.toString(rp.getEntity());
						// We parse whole response and create JSON object,
						// Android
						// JSON implementation
						JSONObject whole = new JSONObject(results);
						// We will check, so there won't be exception, if JSON
						// object has desired key
						if (whole.has("open"))
							return whole.getBoolean("open");
					}
				} catch (Exception e) {
					// Printing out exception stacktrace if anything happens
					e.printStackTrace();
				}
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (status_label != null) {
				if (result) {
					status_label.setText(name + " " + getString(R.string.status_open));
				} else {
					status_label.setText(name + " " + getString(R.string.status_closed));
				}
			}
		}
	}

}
